<?php
$blogtravelhighlightsoneID = get_theme_mod( 'blog_travel_highlights_one','');
$blogtravelcaptionone = get_theme_mod('travel_highlights_caption_one','');
$blogtravelhighlighttwoID = get_theme_mod( 'blog_travel_highlights_two',''); 
$blogtravelcaptiontwo = get_theme_mod('travel_highlights_caption_two','');      
$blogtravelhighlightthreeID = get_theme_mod( 'blog_travel_highlights_three','');
$blogtravelcaptionthree = get_theme_mod('travel_highlights_caption_three','');

$travelHighlight_array = array();
$has_travelHighlight = false;
if( !empty( $blogtravelhighlightsoneID ) ){
	$blog_highlight_one  = wp_get_attachment_image_src( $blogtravelhighlightsoneID,'bosa-420-300');
 	if ( is_array(  $blog_highlight_one ) ){
 		$has_travelHighlight = true;
   	 	$blog_travel_highlight_one = $blog_highlight_one[0];
   	 	$travelHighlight_array['image_one'] = array(
			'ID' => $blog_travel_highlight_one,
			'caption' => $blogtravelcaptionone,
		);	
  	}
}
if( !empty( $blogtravelhighlighttwoID ) ){
	$blog_highlight_two = wp_get_attachment_image_src( $blogtravelhighlighttwoID,'bosa-420-300');
	if ( is_array(  $blog_highlight_two ) ){
		$has_travelHighlight = true;	
        $blog_travel_highlight_two = $blog_highlight_two[0];
        $travelHighlight_array['image_two'] = array(
			'ID' => $blog_travel_highlight_two,
			'caption' => $blogtravelcaptiontwo,
		);	
  	}
}
if( !empty( $blogtravelhighlightthreeID ) ){	
	$blog_highlight_three = wp_get_attachment_image_src( $blogtravelhighlightthreeID,'bosa-420-300');
	if ( is_array(  $blog_highlight_three ) ){
		$has_travelHighlight = true;
      	$blog_travel_highlight_three = $blog_highlight_three[0];
      	$travelHighlight_array['image_three'] = array(
			'ID' => $blog_travel_highlight_three,
			'caption' => $blogtravelcaptionthree,
		);	
  	}
}

if( !get_theme_mod( 'disable_travel_highlights_section', true ) && $has_travelHighlight ){ ?>
	<section class="section-travel-area">
		<div class="content-wrap">
			<div class="row">
				<?php foreach( $travelHighlight_array as $each_travelHighlight ){ ?>
					<div class="col-md-4">
						<article class="travel-content-wrap">
							<figure class= "featured-image">
								<img src="<?php echo esc_url( $each_travelHighlight['ID'] ); ?>">
							</figure>
							<h3 class="entry-title">
								<?php echo esc_html( $each_travelHighlight['caption']  ); ?>
							</h3>
						</article>
					</div>
				<?php } ?>
			</div>	
		</div>
	</section>
<?php } ?>
