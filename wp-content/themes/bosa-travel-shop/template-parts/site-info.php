<?php
/**
 * Template part for displaying site info
 *
 * @package Bosa Travel Shop 1.0.0
 */

?>

<div class="site-info">
	<?php echo wp_kses_post( html_entity_decode( esc_html__( 'Copyright &copy; ' , 'bosa-travel-shop' ) ) );
		echo esc_html( date( 'Y' ) );
		printf( esc_html__( ' Bosa Travel Shop. Powered by', 'bosa-travel-shop' ) );
	?>
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'bosa-travel-shop' ) ); ?>" target="_blank">
		<?php
			printf( esc_html__( 'WordPress', 'bosa-travel-shop' ) );
		?>
	</a>
</div><!-- .site-info -->