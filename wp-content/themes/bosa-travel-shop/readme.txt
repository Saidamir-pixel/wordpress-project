=== Bosa Travel Shop ===
Contributors: bosathemes
Tags: blog, portfolio, news, grid-Layout, one-column, two-columns, three-columns, flexible-header, left-sidebar, right-sidebar, custom-background, custom-colors, custom-logo, custom-menu, featured-images, footer-widgets, full-width-template, post-formats, rtl-language-support, theme-options, sticky-post, threaded-comments, translation-ready, block-styles, wide-blocks
Requires at least: 4.7
Requires PHP: 5.5
Tested up to: 6.0
Stable tag: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Bosa Travel Shop WordPress Theme is child theme of Bosa, Copyright 2022 Bosa Themes
Bosa Travel Shop is distributed under the terms of the GNU General Public License v3

Bosa Travel Shop is based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc.
Underscores is distributed under the terms of the GNU GPL v2 or later.

Bosa Travel Shop is beautiful, fast, lightweight, responsive, extremely customizable, multipurpose theme with beautifully crafted design.

== Description ==

Bosa Travel Shop is multipurpose eCommerce theme. Bosa Travel Shop is beautiful, fast, lightweight, responsive, extremely customizable theme that you can use as a foundation to build versatile sites such as e-commerce, beauty products, women's fashion, smart home products, sports, toy shops, gadgets stores, jewelry shops, men's clothing, electronics, furniture, retail, digital products and preferably ideal for any type of eCommerce sites. Bosa Travel Shop is a child theme of Bosa, a free multipurpose WordPress theme. Bosa Travel Shop works perfectly with Gutenberg and the most popular page builder Elementor that can easily drag-and-drop your ideas from the interface. Bosa Travel Shop is built with SEO, speed, and usability in mind with the multiple Header & Footer layouts, predesigned starter sites includes awesome Home & Inner Pages that is incredibly customizable and powerful enough to take a stand among the crowd. Bosa Travel Shop is compatible with all major plugins like WooCommerce, Yoast, Contact form 7, Mailchimp for WordPress, bbPress, etc. Looking for a Multipurpose eCommerce theme? Look no further! Browse the demo to see that it's the only theme you will ever need: https://demo.bosathemes.com/bosa/travel-shop

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Bosa Travel Shop includes support for WooCommerce, Elementor, Breadcrumb NavXT and Contact From 7.

== Changelog ==

= 1.0.0 =
* Initial Release.

== Resources ==

* Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* Normalize.css http://necolas.github.io/normalize.css [MIT]
* Bootstrap http://getbootstrap.com [MIT]
* Kirki https://kirki.org/ [MIT]
* Slick https://kenwheeler.github.io/slick/ [MIT]
* Slicknav https://github.com/ComputerWolf/SlickNav [MIT]
* Font Awesome http://fontawesome.io/
  Icons — CC BY 4.0 License
  Fonts — SIL OFL 1.1 License
  Code  — MIT License
* Theia Sticky Sidebar https://github.com/WeCodePixels/theia-sticky-sidebar [MIT]
* Image for theme screenshot, Copyright Ylanite Koppens
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://stocksnap.io/license
  Source: https://stocksnap.io/photo/white-sneakers-EA7TDORJBT

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/595899

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/874944

  Image for theme screenshot, Copyright Thoi Trang Gumac 
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/1587545

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/635159

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/940696

  Image for theme screenshot, Copyright katik08
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/1634253

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/635166

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/958791

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/1214751

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/1612756

  Image for theme screenshot, Copyright pxhere
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://pxhere.com/en/license
  Source: https://pxhere.com/en/photo/1619684

  Image for theme screenshot, Copyright Lautaro Andreani
  License: CC0 1.0 Universal (CC0 1.0)
  License URL: https://stocksnap.io/license
  Source: https://stocksnap.io/photo/nature-sunset-RI84WJYDVA


  Logo, Copyright 2022 Bosa Themes
  License: GNU General Public License v3 http://www.gnu.org/licenses/gpl-3.0.html

  Illustrations and icons used in Screenshot, Copyright 2022 Bosa Themes
  License: GNU General Public License v3 http://www.gnu.org/licenses/gpl-3.0.html