<?php 

if(!defined('ABSPATH'))
{
   exit;
}


function footer_style() 
{
 echo '
    <style>
        .footer {margin: 0; padding: 30px; text-align: center; box-shadow: 5px 5px 5px 5px black;}
        .telegram_icon {width: 50px; height: 50px; margin: 0 15px 0 15px;}
        .twitter_icon {width: 50px; height: 50px; margin: 0 15px 0 15px;}
        .mail_icon {width: 50px; height: 50px; margin: 0 15px 0 15px;}
    </style>
 ';
}
