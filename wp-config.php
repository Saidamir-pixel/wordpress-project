<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wptraining' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T0t/%SvWm}q32$jQ0ePy,Z4!.f7X9rjYO5zc;LB%m(^!mOV8h.oCY^9P#2N,{:#%' );
define( 'SECURE_AUTH_KEY',  'sxNXH|2[+uw~RE)QfE6&!:sZ|)}>db70{XN*EHKTBSuN7W=P/EZTXKnIj;R!n3dr' );
define( 'LOGGED_IN_KEY',    'zW[L}BR}~ wi5{AU!!o~uj&xE_&X%3R{>b`JlSppdQH g,~<2DE9ny1/DRkC6m5J' );
define( 'NONCE_KEY',        'yS0{W=FXe N#Gq-xWl^GtGrJVtRk&6knRZjtPa)YD8]TX$hKsl%uQ%|_#j|j4X0o' );
define( 'AUTH_SALT',        'ao-CUl+EM>G9hXD beW5(duc!*_+B]%%%IiX*f1bp6-u>sSEXMgV4qY&j7>e2mFN' );
define( 'SECURE_AUTH_SALT', '}[o;c#}2F@zR6lyA$qr>7=[fZd@Ok=z*]:4fKVbC4P<]ch ca=uMh9AwA+vx=*OA' );
define( 'LOGGED_IN_SALT',   '>RlBDdn}c3.=O6Q5M$$uxI%3)&~^:1,YMYEngbXFiw>>h?N;NU$/xr8/)eak.>D%' );
define( 'NONCE_SALT',       'G?9s8rFx>[m@sp9e-nIRzuU_3l^h@^-;I0iX__<p,M:&[Vl[<=e1~p3rQ:lHL=(D' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
